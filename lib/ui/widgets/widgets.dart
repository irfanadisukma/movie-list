
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movielist/core/controllers/controllers.dart';
import 'package:movielist/core/models/models.dart';
import 'package:movielist/ui/screens/screens.dart';
import 'package:google_fonts/google_fonts.dart';

part 'components/component_product_item.dart';
part 'widget_loading.dart';
part 'components/component_review_item.dart';
part 'components/widget_popup_reviews.dart';
part 'components/component_item_genre.dart';