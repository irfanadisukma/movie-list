part of '../widgets.dart';

class ComponentItemGenre extends ConsumerWidget {
  const ComponentItemGenre({ Key? key, required this.data }) : super(key: key);

  final Genre data;

  @override
  Widget build(BuildContext context, ref) {
    var controller = ref.watch(controllerMovies);
    return InkWell(
      onTap: (){
        controller.movieListData = null;
        controller.currentGenreId = data.id!;
        Navigator.of(context).push(MaterialPageRoute(builder: (context) => ScreenProductList(dataGenre: data)));
      },
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: Colors.black87,
          borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.1),
                spreadRadius: 3,
                blurRadius: 8,
                offset: const Offset(0, 0),
              ),
            ],
        ),
        child: Text(
          data.name!,
          style: GoogleFonts.poppins(
            color: Colors.white,
            fontSize: 16,
            fontWeight: FontWeight.w600,
          )
        )
      ),
    );
  }
}