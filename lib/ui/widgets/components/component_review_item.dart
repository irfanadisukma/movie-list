part of '../widgets.dart';

class ComponentReviewItem extends ConsumerWidget {
  const ComponentReviewItem({Key? key, this.productData}) : super(key: key);

  final ResultReview? productData;

  @override
  Widget build(BuildContext context, ref) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10),
      child: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.1),
                spreadRadius: 3,
                blurRadius: 8,
                offset: const Offset(0, 0),
              ),
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15.0, vertical: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
               (productData!.authorDetails!.name!.isNotEmpty) ? 
               Text(
                  productData!.authorDetails!.name!,
                  maxLines: 2,
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.ellipsis,
                  style: GoogleFonts.poppins(
                    color: Colors.black87,
                    fontSize: 12,
                    fontWeight: FontWeight.w600,
                  )
                ) : const SizedBox(),
              const SizedBox(height: 5),
              Text(productData!.content!,
                textAlign: TextAlign.start,
                style: GoogleFonts.poppins(
                  color: Colors.black87,
                  fontSize: 10,
                  fontWeight: FontWeight.w400,
                )
              ),
            ]
          ),
        )
      ),
    );
  }
}
