part of '../widgets.dart';

class ComponentProductItem extends ConsumerWidget {
  const ComponentProductItem({Key? key, this.productData, this.onClick, this.onClickAddCart}) : super(key: key);

  final Result? productData;
  final Function? onClick;
  final Function? onClickAddCart;

  @override
  Widget build(BuildContext context, ref) {
    return InkWell(
      onTap: () => onClick!(),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(10),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.1),
                  spreadRadius: 3,
                  blurRadius: 1,
                  offset: const Offset(0, 0),
                ),
              ],
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                ClipRRect(
                  borderRadius: const BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10)),
                  child: Image.network(
                    baseUrlImage + productData!.posterPath!,
                    width: MediaQuery.of(context).size.width,
                    height: 150,
                    fit: BoxFit.cover
                  ),
                ),
                const SizedBox(height: 10),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Text(productData!.title!,
                    maxLines: 2,
                    textAlign: TextAlign.start,
                    overflow: TextOverflow.ellipsis,
                    style: GoogleFonts.poppins(
                      color: Colors.black87,
                      fontSize: 14,
                      fontWeight: FontWeight.w700,
                    )
                  ),
                ),
                const SizedBox(height: 4),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10.0),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      const Icon(Icons.star, color: Colors.yellow, size: 14),
                      Text(
                        " ${productData!.voteAverage}", 
                        style: GoogleFonts.poppins(
                          color: Colors.black87,
                          fontSize: 12,
                          fontWeight: FontWeight.w500,
                        )
                      ),
                    ]
                  )
                ),
                const SizedBox(height: 10),
              ]
            )
          ),
      ),
    );
  }
}
