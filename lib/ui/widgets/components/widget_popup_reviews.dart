part of '../widgets.dart';

// ignore: must_be_immutable
class PopupDetailWidget extends ConsumerWidget {
  PopupDetailWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, ref) {
    var productController = ref.watch(controllerMovies);
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
            elevation: 0,
            leading: IconButton(
              icon: const Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () {
                productController.movieDetailData = null;
                productController.movieTrailerData = null;
                Navigator.of(context).pop();
              }
            ),
            centerTitle: false,
            backgroundColor: Colors.black,
            title: Text(productController.movieDetailData!.title!,
              style: GoogleFonts.poppins(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.w700,
              )
            ),
          ),
        body: Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.black.withOpacity(0.3),
                spreadRadius: 2,
                blurRadius: 10,
                offset: const Offset(0, -5), // changes position of shadow
              ),
            ],
            borderRadius: const BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20)),
          ),
          child: (productController.movieReviewData!.results!.isEmpty) ? 
          SizedBox(
            height: 50,
            child: Center(
              child: Text(
                "Belum Ada Review",
                style: GoogleFonts.poppins(
                  color: Colors.black54,
                  fontSize: 10,
                  fontWeight: FontWeight.w500,
                )
              )
            )
          ) : NotificationListener<ScrollUpdateNotification>(
            onNotification: (notification) {
              if (notification.metrics.pixels == notification.metrics.maxScrollExtent) {
                // start loading data
                if (productController.movieReviewData!.results!.length != productController.movieReviewData!.totalResults) {
                  var prevPage = productController.currentPageReview;
                  productController.isLoading = true;
                  productController.currentPageReview = productController.currentPageReview + 1;
                  productController.getReviewByID(movieId: productController.movieReviewData!.id!, prevPage: prevPage, nextPage: productController.currentPageReview);
                } else {
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('You are at the end of the list')));
                }
              }
              return true;
            },
            child: ListView.builder(
              shrinkWrap: true,
              padding: EdgeInsets.zero,
              physics: const ClampingScrollPhysics(),
              scrollDirection: Axis.vertical,
              itemCount: productController.movieReviewData!.results!.length,
              itemBuilder: (BuildContext context, int index) => 
              (productController.movieReviewData!.results![index].authorDetails!.avatarPath!.isNotEmpty) ? 
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: ComponentReviewItem(productData: productController.movieReviewData!.results![index]),
              ) : const SizedBox(),
            ),
          ),
        ),
      ),
    );
  }
}
