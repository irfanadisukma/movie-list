part of 'widgets.dart';

class DialogLoading extends StatelessWidget {
  const DialogLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: const EdgeInsets.symmetric(horizontal: 120),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0.0,
      child: dialogContent(context),
    );
  }

  Widget dialogContent(BuildContext context) {
    return Wrap(
      children: [
        Container(
            padding: const EdgeInsets.all(40),
            child: const Center(
              child: CircularProgressIndicator(
                color: Colors.blue,
                
              ),
            ))
      ],
    );
  }
}
