import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:movielist/core/controllers/controllers.dart';
import 'package:movielist/core/models/models.dart';
import 'package:movielist/ui/widgets/widgets.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

part 'screen_product_list.dart';
part 'screen_product_detail.dart';
part 'screen_review_list.dart';
part 'screen_genre_list.dart';