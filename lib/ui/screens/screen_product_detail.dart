// ignore_for_file: prefer_const_constructors

part of 'screens.dart';

class ScreenProductDetail extends ConsumerStatefulWidget {
  const ScreenProductDetail({Key? key, required this.productData})
      : super(key: key);
  final Result productData;

  @override
  _ScreenProductDetailState createState() => _ScreenProductDetailState();
}

class _ScreenProductDetailState extends ConsumerState<ScreenProductDetail> {
  TextEditingController controllerQuantity = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final productController = ref.watch(controllerMovies);
    if (productController.movieDetailData == null) {
      productController.getMovieByID(movieId: widget.productData.id!);
      productController.getReviewByID(
          movieId: widget.productData.id!,
          prevPage: 1,
          nextPage: productController.currentPageReview);
    }
    return WillPopScope(
      onWillPop: () async {
        productController.movieDetailData = null;
        productController.movieTrailerData = null;
        return true;
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () {
                productController.movieDetailData = null;
                productController.movieTrailerData = null;
                Navigator.of(context).pop();
              }
            ),
            centerTitle: false,
            backgroundColor: Colors.black,
            title: Text(widget.productData.title!,
              style: GoogleFonts.poppins(
                color: Colors.white,
                fontSize: 18,
                fontWeight: FontWeight.w700,
              )
            ),
          ),
          body: productController.movieDetailData != null ? 
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  (productController.movieTrailerData != null) ? 
                  YoutubePlayerIFrame(
                    controller: YoutubePlayerController(
                      initialVideoId: productController.movieTrailerData!.results![0].key!,
                      params: YoutubePlayerParams(
                        startAt: Duration(seconds: 0),
                        showControls: true,
                        showFullscreenButton: true,
                      ),
                    ),
                    aspectRatio: 16 / 9,
                  ) : SizedBox(
                    height: 200,
                    child: Center(child: CircularProgressIndicator())
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Text("Overview",
                      style: GoogleFonts.poppins(
                        color: Colors.black,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      )
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image.network(
                            baseUrlImage + productController.movieDetailData!.posterPath!,
                            height: 150,
                          ),
                        ),
                        SizedBox(width: 10),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: [
                              Text(
                                productController.movieDetailData!.title!,
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontSize: 16,
                                  fontWeight: FontWeight.w600,
                                )
                              ),
                              Divider(),
                              SizedBox(height: 10),
                              Text(
                                "Status:",
                                style: GoogleFonts.poppins(
                                  color: Colors.black,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                )
                              ),
                              SizedBox(height: 10),
                              Text(
                                productController.movieDetailData!.status!,
                                style: GoogleFonts.poppins(
                                  color: Colors.green,
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600,
                                )
                              ),
                            ]
                          ),
                        )
                      ]
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const SizedBox(height: 10),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    const Icon(Icons.star, color: Colors.yellow, size: 14),
                                    Text(
                                      " ${productController.movieDetailData!.voteAverage}",
                                      style: GoogleFonts.poppins(
                                        color: Colors.black87,
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600,
                                      )
                                    ),
                                  ]
                                ),
                                Text("Rating",
                                  style: GoogleFonts.poppins(
                                    color: Colors.grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  )
                                ),
                              ]
                            ),
                            Column(
                              children: [
                                Text(
                                  "${productController.movieDetailData!.releaseDate}",
                                  style: GoogleFonts.poppins(
                                    color: Colors.black87,
                                    fontSize: 16,
                                    fontWeight: FontWeight.w600,
                                  )
                                ),
                                Text(
                                  "Release Date",
                                  style: GoogleFonts.poppins(
                                    color: Colors.grey,
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400,
                                  )
                                ),
                              ]
                            ),
                          ]
                        ),
                        Divider(height: 20),
                        Text(
                          "Deskripsi",
                          style: GoogleFonts.poppins(
                            color: Colors.black87,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          )
                        ),
                        const SizedBox(height: 5),
                        Text(
                          "${productController.movieDetailData!.overview}",
                          style: GoogleFonts.poppins(
                            color: Colors.grey,
                            fontSize: 14,
                            fontWeight: FontWeight.w400,
                          )
                        ),
                        const SizedBox(height: 10),
                        Divider(height: 20),
                        Text(
                          "Reviews",
                          style: GoogleFonts.poppins(
                            color: Colors.black87,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          )
                        ),
                        const SizedBox(height: 5),
                        (productController.movieReviewData!.results!.isEmpty) ? 
                        SizedBox(
                          height: 50,
                          child: Center(
                            child: Text(
                              "Belum Ada Review",
                              style: GoogleFonts.poppins(
                                color: Colors.black54,
                                fontSize: 10,
                                fontWeight: FontWeight.w500,
                              )
                            )
                          )
                        ) : ComponentReviewItem(productData: productController.movieReviewData!.results![0]),
                        const SizedBox(height: 5),
                        (productController.movieReviewData!.results!.isEmpty) ?  SizedBox() : 
                        Center(
                          child: InkWell(
                            onTap: () async {
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) => ScreenReviewList()));
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Text("Lihat Semua",
                                style: GoogleFonts.poppins(
                                  decoration: TextDecoration.underline,
                                  color: Colors.black54,
                                  fontSize: 10,
                                  fontWeight: FontWeight.w500,
                                )
                              ),
                            ),
                          ),
                        ),
                        Divider(height: 30),
                        Text(
                          "RECOMMENDATIONS",
                          style: GoogleFonts.poppins(
                            color: Colors.black87,
                            fontSize: 14,
                            fontWeight: FontWeight.w600,
                          )
                        ),
                        const SizedBox(height: 5),
                      ]
                    ),
                  ),
                  StaggeredGridView.countBuilder(
                    shrinkWrap: true,
                    crossAxisCount: 4,
                    itemCount: 6,
                    physics: const ClampingScrollPhysics(),
                    staggeredTileBuilder: (int index) => const StaggeredTile.fit(2),
                    itemBuilder: (context, index) {
                      return ComponentProductItem(
                        productData: productController.movieListData!.results![index],
                        onClick: () {
                          productController.movieDetailData = null;
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (context) => ScreenProductDetail(productData: productController.movieListData!.results![index])
                            )
                          );
                        }
                      );
                    },
                  ),
                  const SizedBox(height: 60),
                ]
              ),
            ) : Center(child: const CircularProgressIndicator())
          ),
    );
  }
}
