part of 'screens.dart';

class ScreenGenreList extends ConsumerWidget {
  const ScreenGenreList({ Key? key }) : super(key: key);

  @override
  Widget build(BuildContext context, ref) {
    var controller = ref.watch(controllerMovies);
    controller.getGenres();
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        centerTitle: false,
        backgroundColor: Colors.black,
        title: Text("MOVIE GENRE LIST",
          style: GoogleFonts.poppins(
            color: Colors.white,
            fontSize: 18,
            fontWeight: FontWeight.w700,
          )),
      ),
      body: (controller.listGenreData == null) ? const Center(child: CircularProgressIndicator()) : 
        GridView.builder(
          shrinkWrap: true,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
          itemCount: controller.listGenreData!.genres!.length,
          physics: const ClampingScrollPhysics(),
          itemBuilder: (context, index) {
            return ComponentItemGenre(data: controller.listGenreData!.genres![index]);
          },
        ),
    );
  }
}