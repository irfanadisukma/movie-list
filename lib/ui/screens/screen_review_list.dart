// ignore_for_file: prefer_const_constructors

part of 'screens.dart';

class ScreenReviewList extends ConsumerWidget {
  const ScreenReviewList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context, ref) {
   final controller = ref.watch(controllerMovies);
    return WillPopScope(
      onWillPop: () async {
        return true;
      },
      child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            elevation: 0,
            leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.white),
              onPressed: () {
                Navigator.of(context).pop();
              }
            ),
            centerTitle: false,
            backgroundColor: Colors.black,
            title: Text(
              "Reviews:  " + controller.movieDetailData!.title!,
              style: GoogleFonts.poppins(
                color: Colors.white,
                fontSize: 16,
                fontWeight: FontWeight.w700,
              )
            ),
          ),
          body: Stack(
            children: [
              (controller.movieListData != null) ? (controller.movieListData!.results!.isEmpty) ? 
              Container(
                alignment: Alignment.center, 
                margin: const EdgeInsets.only(top: 50.0), child: Text("No Products Found")
              ) : Container(
                margin: const EdgeInsets.only(top: 0.0),
                child: NotificationListener<ScrollUpdateNotification>(
                  onNotification: (notification) {
                    if (notification.metrics.pixels == notification.metrics.maxScrollExtent) {
                      // start loading data
                      if (controller.movieReviewData!.results!.length != controller.movieReviewData!.totalResults) {
                        var prevPage = controller.currentPageReview;
                        controller.isLoading = true;
                        controller.currentPageReview = controller.currentPageReview + 1;
                        controller.getReviewByID(movieId: controller.movieReviewData!.id!, prevPage: prevPage, nextPage: controller.currentPageReview);
                      } else {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('You are at the end of the list')));
                        }
                      }
                      return true;
                    },
                    child: StaggeredGridView.countBuilder(
                      shrinkWrap: true,
                      crossAxisCount: 1,
                      itemCount: controller.movieReviewData!.results!.length,
                      physics: const ClampingScrollPhysics(),
                      staggeredTileBuilder: (int index) => const StaggeredTile.fit(1),
                      itemBuilder: (context, index) {
                        return ComponentReviewItem(
                          productData: controller.movieReviewData!.results![index]);
                        },
                      )),
                    ) : Center(child: const CircularProgressIndicator()),
      
    ])),
    );
  }
}
