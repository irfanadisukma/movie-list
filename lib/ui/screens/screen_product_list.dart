// ignore_for_file: prefer_const_constructors

part of 'screens.dart';

class ScreenProductList extends ConsumerWidget {
  const ScreenProductList({Key? key, required this.dataGenre}) : super(key: key);

  final Genre dataGenre;

  @override
  Widget build(BuildContext context, ref) {
   final controller = ref.watch(controllerMovies);

  if(controller.movieListData == null){
    controller.getProductList(genreId: dataGenre.id , prevPage: 1, nextPage: controller.currentPage);
  }
   
    return WillPopScope(
      onWillPop: () async {
        controller.currentGenreId = 0;
        controller.movieListData = null;
        controller.currentPage = 1; 
        return true;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          centerTitle: false,
          backgroundColor: Colors.white,
          leading: IconButton(
              icon: Icon(Icons.arrow_back, color: Colors.black),
              onPressed: () {
                controller.currentGenreId = 0;
                controller.movieListData = null;
                controller.currentPage = 1; 
                Navigator.of(context).pop();
              }
            ),
            title: Text(
              dataGenre.name!,
              style: GoogleFonts.poppins(
              color: Colors.black,
              fontSize: 18,
              fontWeight: FontWeight.w700,
              )
            ),
          ),
          body: Stack(
            children: [ 
              (controller.movieListData != null) ? (controller.movieListData!.results!.isEmpty) ?
              Container(
                alignment: Alignment.center, margin: const EdgeInsets.only(top: 50.0), child: Text("No Products Found")
              ) : Container(
                margin: const EdgeInsets.only(top: 0.0),
                child: NotificationListener<ScrollUpdateNotification>(
                  onNotification: (notification) {
                    if (notification.metrics.pixels == notification.metrics.maxScrollExtent) {
                      // start loading data
                      if (controller.movieListData!.results!.length != controller.movieListData!.totalResults) {
                        var prevPage = controller.currentPage;
                        controller.isLoading = true;
                        controller.currentPage = controller.currentPage + 1;
                        controller.getProductList(genreId: dataGenre.id, prevPage: prevPage, nextPage: controller.currentPage);
                      } else {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('You are at the end of the list')));
                        }
                      }
                    return true;
                  },
                  child: StaggeredGridView.countBuilder(
                    shrinkWrap: true,
                    crossAxisCount: 4,
                    itemCount: controller.movieListData!.results!.length,
                    physics: const ClampingScrollPhysics(),
                    staggeredTileBuilder: (int index) => const StaggeredTile.fit(2),
                    itemBuilder: (context, index) {
                      return ComponentProductItem(
                        productData: controller.movieListData!.results![index],
                        onClick: () {
                          controller.movieDetailData = null;
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => ScreenProductDetail(productData: controller.movieListData!.results![index])));
                        } 
                        );
                      },
                    )),
                  )
                : Center(child: const CircularProgressIndicator()),
          ]
        )
      ),
    );
  }
}
