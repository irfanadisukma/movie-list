
import 'package:flutter/material.dart';
import 'package:movielist/ui/widgets/widgets.dart';
class Functions {
  static void showLoading(BuildContext context, bool dismissable) {
    showDialog(
      context: context,
      barrierDismissible: dismissable,
      builder: (BuildContext context) => const DialogLoading(),
    );
  }

}