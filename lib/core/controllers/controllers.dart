
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:movielist/core/models/models.dart';
import 'package:http/http.dart' as http;

part 'controller_movies.dart';
part 'values.dart';
