// ignore_for_file: avoid_print

part of 'controllers.dart';

final controllerMovies =
    ChangeNotifierProvider<ControllerProduct>((ref) => ControllerProduct());

class ControllerProduct extends ChangeNotifier {
  ProductListModel? movieListData;
  ProductDetailModel? movieDetailData;
  TrailerModel? movieTrailerData;
  ReviewsModel? movieReviewData;
  ListGenreModel? listGenreData;
  int currentPage = 1;
  int currentPageReview = 1;
  int currentGenreId = 0;
  bool isLoading = false;

  Future getProductList({int? genreId, int? prevPage, int? nextPage}) async {
    try {
      var endpoint = ApiRoute().productListByCategory + "api_key=$apiToken&with_genres=$genreId&page=$nextPage";
      http.Response response = await http.get( Uri.parse(endpoint));

      if(response.statusCode == 200){
         // Checking Page, if current page is not same as the next page, then add new values to list
        if(nextPage != 1 && prevPage != nextPage){
          ProductListModel newData = productListModelFromJson(response.body);
          movieListData!.results!.addAll(newData.results!);
        } else {
          movieListData = productListModelFromJson(response.body);
        }
      } else {
        movieListData = null;
      }
      isLoading = false;
    } catch (e) {
      print(e.toString());
    }
    notifyListeners();
  }


  Future getMovieByID({required int movieId}) async {
    try {
      var endpoint = "${ApiRoute().productById}$movieId?api_key=$apiToken";
      http.Response response = await http.get(Uri.parse(endpoint));
      getMovieTrailerByID(movieId: movieId);
      
      if(response.statusCode == 200){
        movieDetailData = productDetailModelFromJson(response.body);
      } else {
        movieDetailData = null;
      }
      isLoading = false;
    } catch (e) {
      print(e.toString());
    }
  }

  Future getMovieTrailerByID({required int movieId}) async {
    try {
      var endpoint = "${ApiRoute().productById}$movieId/videos?api_key=$apiToken";
      http.Response response = await http.get(Uri.parse(endpoint));
      
      if(response.statusCode == 200){
        movieTrailerData = trailerModelFromJson(response.body);
      } else {
        movieTrailerData = null;
      }
      isLoading = false;
    } catch (e) {
      print(e.toString());
    }
    notifyListeners();
  }

  Future getReviewByID({required int movieId, int? prevPage, int? nextPage}) async {
    try {
      var endpoint = "${ApiRoute().productById}$movieId/reviews?api_key=$apiToken&page=$nextPage";
      http.Response response = await http.get(Uri.parse(endpoint));

      if(response.statusCode == 200){
         // Checking Page, if current page is not same as the next page, then add new values to list
        if(nextPage != 1 && prevPage != nextPage){
          ReviewsModel newData = reviewsModelFromJson(response.body);
          movieReviewData!.results!.addAll(newData.results!);
        } else {
          movieReviewData = reviewsModelFromJson(response.body);
        }
      } else {
        movieReviewData = null;
      }
      isLoading = false;
    } catch (e) {
      print(e.toString());
    }
    notifyListeners();
  }

  Future getGenres() async {
    try {
      var endpoint = "${ApiRoute().genre}api_key=$apiToken";
      http.Response response = await http.get(Uri.parse(endpoint));
      
      if(response.statusCode == 200){
        listGenreData = listGenreModelFromJson(response.body);
        currentGenreId = listGenreData!.genres![0].id!;
      } else {
        listGenreData = null;
      }
      isLoading = false;
    } catch (e) {
      print(e.toString());
    }
    notifyListeners();
  }




}
