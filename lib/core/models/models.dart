import 'dart:convert';

part 'model_movie_list.dart';
part 'model_movie_detail.dart';
part 'model_movie_trailer.dart';
part 'model_reviews.dart';
part 'model_list_genre.dart';