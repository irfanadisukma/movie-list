part of 'models.dart';

ListGenreModel listGenreModelFromJson(String str) => ListGenreModel.fromJson(json.decode(str));

String listGenreModelToJson(ListGenreModel data) => json.encode(data.toJson());

class ListGenreModel {
    ListGenreModel({
        this.genres,
    });

    List<Genre>? genres;

    factory ListGenreModel.fromJson(Map<String, dynamic> json) => ListGenreModel(
        genres: json["genres"] == null ? null : List<Genre>.from(json["genres"].map((x) => Genre.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "genres": genres == null ? null : List<dynamic>.from(genres!.map((x) => x.toJson())),
    };
}

